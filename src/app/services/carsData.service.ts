import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { CarEntity } from "../shared/cars.interface";

@Injectable({
  providedIn: "root",
})
export class CarsService {
  private carsUrl = "api/cars/";
  constructor(private http: HttpClient) {}

  getCars(): Observable<CarEntity[]> {
    return this.http.get<CarEntity[]>(this.carsUrl).pipe(
      retry(2),
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        return throwError(error);
      })
    );
  }

  createCar(car: CarEntity): Observable<CarEntity> {
    return this.http.post<CarEntity>(this.carsUrl, car).pipe(
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        return throwError(error);
      })
    );
  }

  deleteCar(id: number): Observable<CarEntity> {
    return this.http.delete<CarEntity>(this.carsUrl + id);
  }
}
