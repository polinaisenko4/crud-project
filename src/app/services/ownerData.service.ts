import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { OwnerEntity } from "../shared/owners.interface";

@Injectable({
  providedIn: "root",
})
export class OwnerService {
  private ownersUrl = "api/owners/";
  constructor(private http: HttpClient) {}

  getOwners(): Observable<OwnerEntity[]> {
    return this.http.get<OwnerEntity[]>(this.ownersUrl).pipe(
      retry(2),
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        return throwError(error);
      })
    );
  }

  getOwnerById(id: number): Observable<any> {
    return this.http.get(this.ownersUrl + id);
  }

  createOwner(owner: OwnerEntity): Observable<OwnerEntity> {
    return this.http.post<OwnerEntity>(this.ownersUrl, owner).pipe(
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        return throwError(error);
      })
    );
  }

  deleteOwner(id: number): Observable<OwnerEntity> {
    return this.http.delete<OwnerEntity>(this.ownersUrl + id);
  }

  editOwner(owner: any, id: any): Observable<any> {
    return this.http.put(this.ownersUrl + id, owner);
  }
}
