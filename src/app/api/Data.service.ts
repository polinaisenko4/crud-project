import { Injectable } from "@angular/core";
import { InMemoryDbService } from "angular-in-memory-web-api";
@Injectable({
  providedIn: "root",
})
export class DataService implements InMemoryDbService {
  constructor() {}
  createDb() {
    return {
      owners: [
        {
          id: 1,
          aLastName: "Иванов",
          aFirstName: "Иван",
          aMiddleName: "Иванович",
          aCars: [ {
            number: "BB1234AA",
            brand: "KIA",
            model: "Optima",
            year: 2016,
            ownerId: 1,
          }]
        },
        {
          id: 2,
          aLastName: "Олегов",
          aFirstName: "Олег",
          aMiddleName: "Олегович",
          aCars: []
        },
      ],

      cars: [
        {
          id: 1,
          number: "BB1234AA",
          brand: "KIA",
          model: "Optima",
          year: 2016,
          ownerId: 1,
        }
      ]
    };
  }
}
