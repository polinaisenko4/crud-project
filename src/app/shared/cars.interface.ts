import { Observable } from "rxjs";

export interface CarEntity {
  id: number;
  number: string;
  brand: string;
  model: string;
  year: string;
  ownerId: string;
}

export interface ICarService {
  getCars(): Observable<CarEntity[]>;
  getCarsById(aId: number): Observable<CarEntity>;
  createCars(
    number: string,
    brand: string,
    model: string,
    year: string,
    ownerId: string
  ): Observable<CarEntity>;
  editCars(aCars: CarEntity): Observable<CarEntity>;
  deleteCars(aCarsId: number): Observable<CarEntity[]>;
}
