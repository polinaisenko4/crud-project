import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => import('./modules/owners/owners.module').then(m => m.OwnersModule),
  },

  {
    path: 'details',
    loadChildren: () => import('./modules/ownerDetails/owners-details.module').then(m => m.OwnersDetailsModule),
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
