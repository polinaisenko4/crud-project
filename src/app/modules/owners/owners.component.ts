import { Component } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { OwnerService } from "src/app/services/ownerData.service";
import { OwnerEntity } from "src/app/shared/owners.interface";

@Component({
  selector: "app-owner",
  templateUrl: "./owners.component.html",
  styleUrls: ["./owners.component.scss"],
})
export class OwnersComponent {
  view: boolean = false
  edit: boolean = false
  create: boolean = false

  owners!: OwnerEntity[];

  ownerForm!: FormGroup;

  addForm = false;
  addTitle: string = "Добавить";

  constructor(private ownerService: OwnerService, private router: Router) {}

  ngOnInit(): void {
    this.getOwners();
  }

  private getOwners() {
    this.ownerService.getOwners().subscribe((ownersData) => {
      console.log(ownersData);
      this.owners = ownersData;
    });
  }

  onDeleteOwner(id: number) {
    this.ownerService.deleteOwner(id).subscribe();
    this.getOwners();
  }

  redirectToCreateOwner() {
    this.router.navigate(["/details"], {
      queryParams: {
        create: true,
      },
    }),
      () => console.log("Redirect success");
  }

  redirectToViewOwner(id: any) {
    this.router.navigate(["/details"], {
      queryParams: {
        view: true,
        id: id
      },
    }),
      () => console.log("Redirect success");
  }

  redirectToEditOwner() {
    this.router.navigate(["/details"], {
      queryParams: {
        edit: true,
      },
    }),
      () => console.log("Redirect success");
  }
}
