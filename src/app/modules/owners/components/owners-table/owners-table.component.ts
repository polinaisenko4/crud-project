import { Component, Input } from "@angular/core";

@Component({
  selector: "app-owners-table",
  templateUrl: "./owners-table.component.html",
  styleUrls: ["./owners-table.component.scss"],
})
export class OwnersTableComponent {
  @Input() owner!: any;
}
