import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { OwnersTableComponent } from "./components/owners-table/owners-table.component";
import { OwnersRoutingModule } from "./owners-routing.module";
import { OwnersComponent } from "./owners.component";

@NgModule({
  declarations: [

    OwnersComponent,
    OwnersTableComponent,
  ],
  imports: [
    CommonModule,
    OwnersRoutingModule
  ],
  bootstrap: [OwnersComponent]
  
})
export class OwnersModule { }

