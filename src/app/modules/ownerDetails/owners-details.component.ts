import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { CarsService } from "src/app/services/carsData.service";
import { OwnerService } from "src/app/services/ownerData.service";
import { OwnerEntity } from "src/app/shared/owners.interface";

@Component({
  selector: "app-owner-details",
  templateUrl: "./owners-details.component.html",
  styleUrls: ["./owners-details.component.scss"],
})
export class OwnersDetailsComponent implements OnInit {
  view: boolean = false;
  edit: boolean = false;
  create: boolean = false;
  readOn: any = false;
  value: any;

  ownerForm!: FormGroup;
  carForm!: FormGroup;
  owner!: OwnerEntity;
  ownerById!: OwnerEntity;

  addForm!: boolean;
  addTitle!: string;
  v: boolean = false;
  ownerId!: number;
  addCar: boolean = false;

  constructor(
    private ownerService: OwnerService,
    private carsService: CarsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params: Params) => {
      if (params["create"]) {
        this.readOn = false;
        this.create = true;
        this.view = false;
        this.edit = false;

        console.log("Create", this.readOn);
        console.log(this.v);
      } else if (params["view"]) {
        this.readOn = true;
        this.create = false;
        this.view = true;
        this.edit = false;
        this.ownerId = params["id"];

        this.ownerService.getOwnerById(this.ownerId).subscribe((res) => {
          this.ownerById = res;
        });
      } else if (params["edit"]) {
        this.readOn = false;
        this.edit = true;
        this.create = false;
        this.view = false;
        this.edit = true;

        this.ownerId = params["id"];

        this.ownerService.getOwnerById(this.ownerId).subscribe((res) => {
          this.ownerById = res;
        });
        
        console.log("Edit", this.readOn);
      }
    });

    this.ownerForm = new FormGroup({
      aLastName: new FormControl("", [Validators.required]),
      aFirstName: new FormControl("", [Validators.required]),
      aMiddleName: new FormControl("", [Validators.required]),
    });

    this.carForm = new FormGroup({
      number: new FormControl("", [Validators.required]),
      brand: new FormControl("", [Validators.required]),
      model: new FormControl("", [Validators.required]),
      year: new FormControl("", [Validators.required]),
    });
  }

  onAddOwner() {
    const owner: any = {
      aLastName: this.ownerForm.value.aLastName,
      aFirstName: this.ownerForm.value.aFirstName,
      aMiddleName: this.ownerForm.value.aMiddleName,
      aCars: [],
    };

    if (this.ownerId === undefined) {
      this.ownerService.createOwner(owner).subscribe((res) => {
        this.ownerId = res.id;
        this.addCar = true;
        this.owner = res;
        console.log(res);
      });
    } else {
      this.ownerService.getOwnerById(this.ownerId).subscribe();
    }
    // this.ownerForm.reset();
  }

  onAddCar() {
    const car: any = {
      number: this.carForm.value.number,
      brand: this.carForm.value.brand,
      model: this.carForm.value.model,
      year: this.carForm.value.aMiddleName,
      ownerId: this.ownerId,
    };

    this.carsService.createCar(car).subscribe((res) => {
      this.owner.aCars.push(res);
      console.log(this.owner);
      this.ownerService.editOwner(this.owner, this.ownerId).subscribe();
    });

    this.carForm.reset();
  }

  redirectOnMainPage() {
    //   if (this.create === true) {
    //     if (this.owner.aCars.length === 0) {
    //       this.ownerService.deleteOwner(this.ownerId).subscribe();
    //     }
    //     this.router.navigate(["/"]);
    //   }

    //   if (this.view === true) {
    //     this.router.navigate(["/"]);
    //   }
    // }
    this.router.navigate(["/"]);
  }

  ondeleteCar(id: any) {
  

     this.carsService.deleteCar(id).subscribe(() => {
       this.ownerService.getOwnerById(this.ownerId).subscribe( (res) => console.log(res, this.ownerById))
     })
  }
}
