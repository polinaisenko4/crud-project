import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { OwnersDetailsRoutingModule } from "./owners-details-routing.module";
import { OwnersDetailsComponent } from "./owners-details.component";

@NgModule({
  declarations: [
    OwnersDetailsComponent
  ],
  imports: [
    CommonModule, 
    ReactiveFormsModule,
    OwnersDetailsRoutingModule
  
  ],

  bootstrap: [OwnersDetailsComponent],
})
export class OwnersDetailsModule {}
